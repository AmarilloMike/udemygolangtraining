package main

import (
	"encoding/json"
	"fmt"
)

type person struct {
	First       string // UPPER CASE Field = Exported!!!!
	Last        string
	Age         int
	notExported int  // Lower case NOT Exported!!!
}

func main() {
	p1 := person{"James", "Bond", 20, 007}
	bs, _ := json.Marshal(p1)
	fmt.Println(bs)
	fmt.Printf("%T \n", bs)
	fmt.Println(string(bs))
	// Only EXPORTED Fields are processed by 'Marshal'
	// Uppercase first letter of field name signals 'Exported'!!!
}
