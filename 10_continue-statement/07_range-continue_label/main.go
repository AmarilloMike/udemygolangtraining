package main

import "fmt"

func main() {
	xs := []int{1, 2, 3, 4, 5, 6, 7}

OUTLOOP:

	for i := 0; i < 2; i++ {

		for _, value := range xs {

			if value == 3 {

				continue OUTLOOP
			}

			fmt.Println("i=", i, " - range value=", value)

		}

	}
}
