package main

import "fmt"

func main() {

	var y float64 = 13.0 / 3.0  // Division

	fmt.Println("Result of Division ", y)

	x := 13 % 3  // Reminder modulo
	fmt.Println("Remainder is ", x)
	if x == 1 {
		fmt.Println("Odd")
	} else {
		fmt.Println("Even")
	}

	for i := 1; i < 70; i++ {
		if i%2 == 1 {
			fmt.Println("Odd")
		} else {
			fmt.Println("Even")
		}
	}
}
