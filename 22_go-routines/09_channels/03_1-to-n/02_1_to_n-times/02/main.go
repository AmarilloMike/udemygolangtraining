package main

import (
	"fmt"
)

// Put 0-19 on the channel. Then launch n (10) subroutines
// to read and print the channel ints. This prints 0-19
// non-sequentially.
func main() {

	n := 10
	c := make(chan int)
	done := make(chan bool)

	go func() {
		for i := 0; i < 20; i++ {
			c <- i
		}
		close(c)
	}()

	for i := 0; i < n; i++ {
		go func() {
			for n := range c {
				fmt.Println(n)
			}
			done <- true
		}()
	}

	for i := 0; i < n; i++ {
		<-done
	}
}
