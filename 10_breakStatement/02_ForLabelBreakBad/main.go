package main

import (
	"fmt"
)

func main() {

	var cnt = 0

REDO:

	for i := 0; i < 20; i++ {

		if i == 4 {
			cnt++
			break REDO
		}

		if cnt == 3 {
			break LAST
		}

		fmt.Println("i=", i)

	}

LAST:
	fmt.Println("Last value of cnt =", cnt)

}
