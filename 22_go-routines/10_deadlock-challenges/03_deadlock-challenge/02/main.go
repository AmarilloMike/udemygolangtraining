package main
// 03-02 Solution

import (
	"fmt"
)

func main() {
	c := make(chan int)
	n := 10

	go func() {
		for i := 0; i < n; i++ {
			c <- i
		}
		close(c)
	}()

	for i := range c {
		fmt.Println(i)
	}

}

// Challenge - see 01
// Why does this only print zero?
// And what can you do to get it to print all 0 - 9 numbers?

// This is solution. It works.