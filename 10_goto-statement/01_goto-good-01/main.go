package main

import "fmt"

func main() {
	x := "Hello"

	if x == "Hello" {
		x = "Going To EXITFunc!"
		goto EXITFunc
	}

	x = "Hello Mike"

EXITFunc:

	fmt.Printf(x)
}
