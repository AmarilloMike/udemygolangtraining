package main

import "fmt"

func main() {

	x := "Hello"

	if x == "Hello" {
		goto BadInsideLoop
	}

	for i := 0; i < 5; i++ {

	BadInsideLoop:

		fmt.Println("i=", i)
	}

	fmt.Println("End Of Program")

}
