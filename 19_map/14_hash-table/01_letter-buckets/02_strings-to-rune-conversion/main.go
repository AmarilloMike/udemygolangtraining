package main

import "fmt"

func main() {
	// rune is alias for int32
	// For this string give me a
	// 	a slice consisting of the
	//	first letter
	letter := rune("A"[0])
	fmt.Println(letter)
}
