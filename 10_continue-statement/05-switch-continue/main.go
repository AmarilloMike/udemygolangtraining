package main

import "fmt"

func main() {

	for i := 0; i < 5; i++ {

		switch i {
		case 0, 1, 2:
			fmt.Println("i=", i)
		case 3, 4:
			continue
		default:
			fmt.Println("i=", i)

		} // End of switch statement

	} // End of i loop

	fmt.Println("End Of Program Execution!")
}
