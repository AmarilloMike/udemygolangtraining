package main

import "fmt"

func main() {
	fmt.Println(greet("Jane ", "Doe"))
}

func greet(fname, lname string) string {
	return fmt.Sprint(fname, lname)
}

// Sprint = String Print
// fmt.Sprint(fname, lname) returns one string
// which is a concatenation of fname and lname.

