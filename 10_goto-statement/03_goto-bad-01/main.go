package main

import "fmt"

func main() {

	for i := 0; i < 5; i++ {

		if i == 3 {

			goto EXITFunc

		}

		fmt.Println("i=", i)
	}

	y := "Exiting via return statement"
	fmt.Println(y)

	return

EXITFunc:

	fmt.Println("Exiting via EXITFunc")
}
