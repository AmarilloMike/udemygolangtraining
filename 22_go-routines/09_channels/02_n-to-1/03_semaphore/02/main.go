package main

import (
	"fmt"
)

var c chan int
var done chan bool

func main() {

	c = make(chan int)
	done = make(chan bool)

	go Add01_1()

	go Add01_2()

	go WaitForIt()

	for n := range c {
		fmt.Println(n)
	}
}

func Add01_1()  {

	for i := 0; i < 10; i++ {
		c <- i
	}
	done <- true
}

func Add01_2()  {
	for i := 0; i < 10; i++ {
		c <- i
	}

	done <- true
}

func WaitForIt()  {
	<-done
	<-done
	close(c)
}