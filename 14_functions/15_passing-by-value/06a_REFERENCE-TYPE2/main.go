package main

import "fmt"

func main() {
	m := make([]string, 1, 25)
	m[0] = "Cy"
	fmt.Println(m) // 1. [Cy]
	changeMe(m)
	fmt.Println(m) // 3. [Todd]
}

func changeMe(z []string) {
	z[0] = "Todd"
	fmt.Println(z) // 2. [Todd]
}

