package main

import "fmt"

func main() {
RowLoop:
	for i := 0; i < 3; i++ {

		for j := 0; j < 7; j++ {
			if j == 2 {
				continue RowLoop // continue with label
			}

			fmt.Println("i=", i, "  j=", j)
		} // End of j Loop

	} // End of i Loop

	fmt.Println("End of Program Exectution!")
}
