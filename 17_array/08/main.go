package main

import "fmt"

// Example of a 2-dimensional array
func main() {
	var twoD [2][3]int
	for i := 0; i < 2; i++ {
		for j := 0; j < 3; j++ {
			twoD[i][j] = i + j
		}
	}
	fmt.Println("2d: ", twoD)
	fmt.Println("Cell 0,0=", twoD[0][0])
	fmt.Println("Cell 0,1=", twoD[0][1])
	fmt.Println("Cell 0,2=", twoD[0][2])
	fmt.Println("Cell 1,2=", twoD[1][2])
}
