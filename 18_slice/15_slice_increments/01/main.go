package main

import "fmt"

func main() {
	mySlice := make([]int, 1)
	fmt.Println(mySlice[0])
	mySlice[0] = 7
	fmt.Println(mySlice[0]) // Prints 7
	mySlice[0]++
	fmt.Println(mySlice[0]) // Prints 8
	mySlice[0] += 1
	fmt.Println(mySlice[0])  // Prints 9
}
