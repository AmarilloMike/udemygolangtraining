package main

import "fmt"

func main() {
	// Print with formatting
	// Printf formats according to a format specifier and
	// writes to standard output.
	// https://godoc.org/fmt
	// %d = decimal
	// %b = binary
	//fmt.Printf("%d - %b", 42, 42)
	// Adding a new line escape character
	fmt.Printf("%d - %b\n", 42, 42)
}
