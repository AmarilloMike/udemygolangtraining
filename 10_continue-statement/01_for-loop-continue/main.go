package main

import "fmt"

func main() {

	for i := 0; i < 6; i++ { // Top of for loop

		if i%2 == 0 {
			continue
		}

		fmt.Println(i)

	} // End of for loop

	fmt.Println("Program Execution Completed!")
}
