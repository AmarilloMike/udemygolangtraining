package main

import (
	"fmt"
)

func main() {
	greeting := []string{
		"Good morning!",
		"Bonjour!",
		"dias!",
		"Bongiorno!",
		"Ohayo!",
		"Selamat pagi!",
		"Gutten morgen!",
	}
	// Trailing comma required here!


	for i, currentEntry := range greeting {
		fmt.Println(i+1,". ",i, currentEntry)
	}

	fmt.Println("-------------------------------")

	for j := 0; j < len(greeting); j++ {
		fmt.Println(j+1,". ",greeting[j])
	}

	fmt.Println("-------------------------------")
	fmt.Println("Greeting initialized to 7-strings")
	fmt.Println("greeting length:",len(greeting), "  capacity:", cap(greeting))
}
