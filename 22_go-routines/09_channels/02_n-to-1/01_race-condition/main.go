package main

import (
	"fmt"
	"sync"
)

// This is a 'race' condition!!!
func main() {

	c := make(chan int)

	var wg sync.WaitGroup

	go func() {
		wg.Add(1) // <-- Attempting to access shared variable from
				  	// two different goroutines!
		for i := 0; i < 10; i++ {
			c <- i
		}
		wg.Done()
	}()

	go func() {
		wg.Add(1) // <-- Attempting to access shared variable from
					// two different goroutines!
		for i := 0; i < 10; i++ {
			c <- i
		}
		wg.Done()
	}()

	go func() {
		wg.Wait()
		close(c)
	}()

	for n := range c {
		fmt.Println(n)
	}
}
