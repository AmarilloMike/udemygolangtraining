package main

import "fmt"

func main() {
	xs := []int{1, 2, 3, 4, 5, 6, 7}

	for i, value := range xs {

		// Print value of i if value of i is an odd number
		if value%2 == 0 {
			// Skip all even numbers
			continue
		}

		fmt.Println("idx=", i, " - odd value=", value)

	}
}
