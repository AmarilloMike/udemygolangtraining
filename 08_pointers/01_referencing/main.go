package main

import (
	"fmt"
)

func main() {

	a := 43

	fmt.Println(a)  // 43
	fmt.Println(&a) // 0xc08200e2b0

	// var b = &a -- This also works
	var b *int = &a  // golint suggests omitting *int as it will
					 //   be inferred.

	fmt.Println(b) // 0xc08200e2b0
	// Prints out the hex memory location of a

	fmt.Println(*b) // 43
	// 'dereferences' the pointer and prints value of b

	// the above code makes b a pointer to the memory address where an int is stored
	// b is of type "int pointer"
	// *int -- the * is part of the type -- b is of type *int
}
