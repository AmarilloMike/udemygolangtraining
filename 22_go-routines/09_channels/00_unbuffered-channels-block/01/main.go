package main

import (
	"fmt"
	"time"
)

func main() {
	c := make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			c <- i // blocks until value taken off channel
		}
	}()

	go func() {
		for {
			fmt.Println(<-c)  // blocks until value put on channel
		}
	}()

	time.Sleep(time.Second)
}
