package main

import "fmt"

func main() {

	xs := []int{1, 2, 3, 4, 5, 6}

	cycleCnt := 0

OUTLOOP:

	for {	// forever loop

		for i, value := range xs {

			if value == 4 {
				break OUTLOOP
			}

			fmt.Println(i, " - ", value)
			cycleCnt++

		} // end of range loop

	} // end of forever loop

	fmt.Println("cycleCnt Value =", cycleCnt)
}
