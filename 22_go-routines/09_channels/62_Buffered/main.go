package main

import (
	"fmt"
	"time"
)

func main() {

	// buffered channel, will accept 10-things before
	// 	it is full.
	c := make(chan int, 10)


	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}
	}()

	go func() {
		for {
			fmt.Println(<-c)
		}
	}()

	time.Sleep(2 * time.Second)
}
