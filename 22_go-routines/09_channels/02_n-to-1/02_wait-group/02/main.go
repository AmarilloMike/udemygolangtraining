package main

/*
	This example is an alternate wait group solution
*/

import (
	"fmt"
	"sync"
)

var c chan int

var wg sync.WaitGroup

func main() {

	c = make(chan int)

	wg.Add(2)

	go Add10_1()

	go Add10_2()

	go WaitForIt()

	for n := range c {
		fmt.Println(n)
	}
}

// Add10_1 sends 0 through 9 to channel.
func Add10_1() {
	for i := 0; i < 10; i++ {
		c <- i
	}
	wg.Done()
}

// Add10_2 sends 0 through 9 to channel.
func Add10_2() {

	for i := 0; i < 10; i++ {
		c <- i
	}
	wg.Done()

}

// WaitForIt waits for all go goroutines to finish
// and then closes channel.
func WaitForIt() {
	wg.Wait()
	close(c)
}

/*	Output - Note: Sequence may vary with each run
	$ go run main.go
	0
	1
	0
	2
	1
	3
	2
	4
	3
	5
	4
	6
	5
	7
	6
	8
	7
	9
	8
	9
*/
