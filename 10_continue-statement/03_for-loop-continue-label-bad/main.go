package main

import "fmt"

func main() {

	for i := 0; i < 3; i++ {

		for j := 0; j < 7; j++ {
			if j == 2 {
				// type of label will throw a runtime error
				continue ExitLoop
			}

			fmt.Println("i=", i, "  j=", j)
		} // End of j Loop

	} // End of i Loop

ExitLoop:
	fmt.Println("End of Program Exectution!")
}
