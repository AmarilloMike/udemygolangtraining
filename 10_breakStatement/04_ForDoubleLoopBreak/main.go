package main

import "fmt"

func main() {

	var valJ = 0
	var valI = 0

	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			if j == 2 {
				break
			}

			valJ = j
			fmt.Println("j=", j)
		} // End of j loop

		valI = i
		fmt.Println("i=", i)
	} // End of i loop

	fmt.Println("Value of j =", valJ)
	fmt.Println("Value of i =", valI)

}
