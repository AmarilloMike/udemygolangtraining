package main

import (
	"fmt"
)

func main() {
	var result = 0
	for i := 0; i < 10; i++ {

		fmt.Println("i=", i)

		if i == 5 {
			result = i
			break
		}
	}

	fmt.Println("Last value of i after for loop:", result)
}
