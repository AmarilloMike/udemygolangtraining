package main

import "fmt"

/*
	This example demonstrates the builtin function 'copy'.
	Slice 's' is copied to slice 't'
*/
func main() {
	s := make([]string, 3)
	s[0] = "a"
	s[1] = "b"
	s[2] = "c"
	fmt.Println("This is s1", s)
	t := make([]string, len(s))
	copy(t, s)
	fmt.Println("s:", s)
	fmt.Println("t:", t)
	t[0] = "1"
	t[1] = "2"
	t[2] = "3"
	fmt.Println("s:", s)
	fmt.Println("t:", t)

}

/*	Output
	$ go run main.go
	This is s1 [a b c]
	s: [a b c]
	t: [a b c]
	s: [a b c]
	t: [1 2 3]
*/
