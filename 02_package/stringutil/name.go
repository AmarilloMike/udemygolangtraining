package stringutil


// MyName will be exported because it starts with a capital letter
//var MyName string = "Mike" //Option: Could specify type

var MyName = "Mike"