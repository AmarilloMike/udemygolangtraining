package main

import "fmt"

func main() {

	var i = 0

OUTLOOP:

	for { // forever loop

		i++

		switch i {

		case 5:
			break OUTLOOP

		default:
			fmt.Println("value of i =", i)

		} // End of switch statement

	} // End of for loop

	fmt.Println("Execution Finished!")
	fmt.Println("Value of i =", i)
}
