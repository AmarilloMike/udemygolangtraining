package main

import "fmt"

func main() {

	var i = 0

	for { // forever for loop

		i++

		switch i {

		case 5:
			break

		default:
			continue
		}

		break // break out of for loop
	}

	fmt.Println("Execution Finished!")
	fmt.Println("Value of i =", i)
}
