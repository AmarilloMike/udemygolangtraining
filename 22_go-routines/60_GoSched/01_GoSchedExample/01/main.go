package main

// Mastering Concurrency In Go
// D:\GitRepo2\MasterConcurrencyInGo\3483OS_Code\3483OS_01_Codes\ch1_4_scheduler2.go

// Note: using GoSched does not always allow time for all goroutines
// to finish.
import(
	"fmt"
	"runtime"
)

func showNumber(num int) {
	fmt.Println(num)
}

func main() {
	iterations := 10

	for i := 0; i<=iterations; i++ {

		go showNumber(i)

	}

	runtime.Gosched()

	fmt.Println("Goodbye!")

}
